package com.camunda.orderpizza.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author Salyakhov Amir
 * Project order-pizza
 * Created 28.05.2022
 */

public class LogDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println(delegateExecution.getVariables());
    }
}
