# Урок 1. Запуск Camunda

***
## Условные обозначения

__*Курсив*__ указывает на новые термины

``` для листинг кода ```

__жирный текст то что должен ввести пользователь__
***

## __*Задачи*__

- Запустить приложение
- Превратить __*Task(Задача)*__ в __*Service Task*__ (Сервисная задача) 
- Подключить код LogDelegate.java выполняющий логирование в консоль наших REST запросов

***
## __*Основы*__
* Запуск Camunda
* Создание простой диаграммы __BPMN__ заказа пиццы
* Знакомство с __*Service Task*__
* Исполнение кода с помощью __*Java Delegate*__
* Логирование в командную строку 

## Запуск Camunda
В данном уроке Вам предстоит запустить Camunda на своём компьютере, приложение уже настроено и готово к использованию.
После запуска приложения откройте в браузере http://localhost:8080 используя логин __demo__ и пароль __demo__,
вы увидите следующую страницу.
![img.png](readmeFiles/img.png)
Можете потыкаться и изучить интерфейс)

## ~~Создание~~ Редактирование  простой диаграммы __BPMN__
В Вашем проекте уже имеется простейшая диаграмма __Order_pizza.bpmn__ в каталоге __*src/main/resources/processes*__
которую мы будем дополнять логикой.

![img_1.png](readmeFiles/simpleBPMN.png)

## Знакомство с __*Service Task*__
+ Сервисная задача(__*[Service Task](https://docs.camunda.org/manual/7.17/reference/bpmn20/tasks/service-task/)*__) используется для вызова сервисов. В Camunda это делается путем вызова кода Java
  или предоставления рабочего элемента для внешнего исполнителя для асинхронного завершения или вызова логики, реализованной в форме веб-сервисов.

![img.png](readmeFiles/serviceTask.png)

- Шаг 1 превращаем наш Task в Service Task. Открываем нашу диаграмму с помощью Camunda Modeler и делаем всё как на изображении(незабываем сохранять изменения).

![img.png](readmeFiles/task-to-serviceTask.png)

## Исполнение кода с помощью Java Delegate

__*[Java Delegate](https://docs.camunda.org/manual/7.17/user-guide/process-engine/delegation-code/)*__ применяется для исполнения класса, который можно вызывать во время выполнения процесса, 
этот класс должен [имплементировать](https://javarush.ru/quests/lectures/questsyntaxpro.level17.lecture06)(реализовывать) интерфейс org.camunda.bpm.engine.delegate.JavaDelegate 
и предоставить необходимую логику в методе execute. Когда выполнение процесса дойдет до этого конкретного шага,
он выполнит эту логику, определенную в этом методе, и оставит действие в соответствии с BPMN 2.0 по умолчанию.
```
public class LogDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println(delegateExecution.getVariables());
    }
}
```
- Шаг 2 добавляем код в *Service Task* Открываем нашу диаграмму с помощью Camunda Modeler и делаем всё как на изображении, где:
  + 1 -- мы говорим нашей BPMN схеме, что хотим исполнить Java класс. 
  + 2 -- мы указываем путь и название нашего Java класса, который будет исполнять Camunda. 
  + 3 -- не забываем о документации :)(незабываем сохранить наши изменения)
![img.png](readmeFiles/addJavaClass.png)

## Логирование в командную строку

Для завершающего шага нам необходимо отправить POST запрос в web-сервис с помощью Postman
Адрес куда отправляем Post запрос: http://localhost:8080/engine-rest/process-definition/key/Opder_pizza/start

JSON для нашего запроса 
```
{
  "variables": {
    "customer": {
      "value": "Донателло",
      "type": "String"
    },
    "address": {
      "value": "Нью-Йорк",
      "type": "String"
    },
    "pizza": {
      "value": "Пицца с моцареллой, ветчиной и сладким перцем, и никаких анчоусов!",
      "type": "String"
    }
  },
  "businessKey": "Process_LoggerExample",
  "withVariablesInReturn": true
}
```

#### Дополнительная информация:
Конфигурация приложения указана в файле application.yaml, ниже расшифровка полей из файла
```
camunda:
  bpm:
    admin-user:
      id: demo                  /*Логин для входа в Camunda*/
      password: demo            /*Пароль для входа в Camunda*/
      first-name: SuperUser     /*Это ты!*/
    filter:
      create: All task

server:
  port: 8080                    /*Порт по умолчанию для запуска приложения*/
```